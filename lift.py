def scorer(y_true, y_predicted, display_results = True):
    """ Отрисовывает графики качества и значения метрик lift и GAIN
        args: y_predicted: type(y_predicted) = np.array - значение вероятностей быть ушедшим абонентом
              y_true: type(y_true) = np.array - значение меток класса
        return:  liftK - значение метрики liftK
    """
    if display_results:
        print 'Confusion matrix: '
        print metrics.confusion_matrix(y_true, np.round(y_predicted))
    
    inx = range(len(y_predicted))
    def comparator(x, y):
        if y_predicted[x] != y_predicted[y]:
            return int(-int((y_predicted[x] - y_predicted[y]) * 100000))
        else:
            return int(-y_true[x] + y_true[y])
    inx.sort(comparator)
    y_predicted = y_predicted[inx]
    y_true = y_true[inx]
    gain = np.cumsum(y_predicted, axis=0) / y_predicted.sum()
    x_axe = np.ones((len(y_predicted),))
    x_axe = np.cumsum(x_axe, axis=0) / x_axe.sum()
    x_points = np.arange(0, 100.0001, 0.5)
    lift = []
    for xpoint in x_points:
        first_part = y_true[0:(int(len(y_true) * xpoint / 100))]
        lift.append((first_part.sum() / float(len(first_part))) / (y_true.sum() / float(len(y_true))))
    if display_results:
        print 'Lift10 = ', lift[np.where(x_points == 10)[0][0]]
        fig = plt.figure(figsize=(8, 8))
        ax = fig.add_subplot(111)
        ax.plot(x_points, lift, label='Sorted by churn probability')
        ax.plot(x_points, np.ones((len(x_points),) ), label='Random baseline')
        ax.legend(loc='upper center')
        ax.set_ylabel("Lift value", fontsize=18)
        ax.set_xlabel("Customer persent", fontsize=18)
        ax.set_title("Lift chart", fontsize=25)
        ax.set_xticks(np.arange(0, 100.0001, 5))
        ax.tick_params(axis = 'x', which = 'major', labelsize = 10)
        ax.axis([0, 100, 0, np.nanmax(lift) + 0.5])
        #ax.get_figure().savefig('Lift_plot.jpeg')
        plt.show()
        print 'GainArea = ', (gain.sum() - 1.0 / 2.0) / float(len(gain))
        fig = plt.figure(figsize=(8, 8))
        ax = fig.add_subplot(111)
        ax.plot(x_axe, gain, label='Sorted by churn probability')
        ax.plot(x_axe, x_axe, label='Random baseline')
        ax.legend(loc='upper center')
        ax.set_ylabel("Churn customer percent", fontsize=18)
        ax.set_xlabel("Customer persent", fontsize=18)
        ax.set_title("Gain chart", fontsize=25)
        ax.set_xticks(np.arange(0, 1.0001, 0.05))
        ax.set_yticks(np.arange(0, 1.0001, 0.05))
        ax.tick_params(axis = 'both', which = 'major', labelsize = 8)
        #ax.get_figure().savefig(Gain_plot.jpeg')
        plt.show()
    return lift[np.where(x_points == 10)[0][0]]
